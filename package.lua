return {
  name = "Bilal2453/some-fancy-package",
  version = "0.0.1",
  description = "A simple git repo to test lit install command.",
  tags = { "lua", "lit", "luvit" },
  license = "MIT",
  author = { name = "Bilal2453", email = "belal2453@gmail.com" },
  homepage = "https://gitlab.com/Bilal2453/lit-git-test",
  dependencies = { "https://github.com/SinisterRectus/discordia" },
  files = {
    "**.lua",
    "!test*",
    "!bin",
  }
}
